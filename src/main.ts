const questions = [

    {
        question: "Quelle est la planète la plus froide de notre système solaire ?",
        answers: [
            { text: "Uranus", correct: false },
            { text: "Pluton", correct: false },
            { text: "Neptune", correct: true },
            { text: "Mercure", correct: false },
        ]
    },
    {
        question: "Quelle est la différence entre une comète et un astéroïde ?",
        answers: [
            { text: "Comètes : roche et métal, Astéroïdes : glace et poussière", correct: false },
            { text: "Comètes : glace et poussière, Astéroïdes : roche et métal", correct: true },
            { text: "Comètes : trou noir, Astéroïdes : supernova", correct: false },
            { text: "Comètes : météorite, Astéroïdes : astéroïde", correct: false },
        ]
    },
    {
        question: "Quelle est la plus grande lune de notre système solaire ?",
        answers: [
            { text: "Titan", correct: false },
            { text: "Io", correct: false },
            { text: "Europe", correct: false },
            { text: "Ganymède", correct: true },
        ]
    },
    {
        question: "Nom de la mission spatiale de la NASA étudiant Jupiter et ses lunes ?",
        answers: [
            { text: "Galileo", correct: true },
            { text: "Juno", correct: false },
            { text: "Pioneer", correct: false },
            { text: "Voyager", correct: false },
        ]
    },
    {
        question: "Qu'est-ce que la ceinture de Kuiper ?",
        answers: [
            { text: "Une ceinture d'astéroïdes entre Mars et Jupiter.", correct: false },
            { text: "Une région du système solaire au-delà de l'orbite de Neptune, remplie de petits corps glacés.", correct: true },
            { text: "Une bande de poussière et de gaz dans le centre de la Voie lactée.", correct: false },
            { text: "Une région de forte activité sismique sur Mars.", correct: false },
        ]
    },
    {
        question: "Quelle est la plus grande étoile connue dans l'univers ?",
        answers: [
            { text: "Sirius", correct: false },
            { text: "Le Soleil", correct: false },
            { text: "VY Canis Majoris", correct: true },
            { text: "Proxima Centauri", correct: false },
        ]
    },
    {
        question: "Qu'est-ce que l'effet de lentille gravitationnelle ?",
        answers: [
            { text: "Une déformation de l'espace-temps causée par la présence de matière, qui dévie la lumière des objets lointains.", correct: true },
            { text: "L'effet de flou observable lors de l'imagerie d'objets astronomiques à l'aide de télescopes terrestres.", correct: false },
            { text: "Un phénomène optique résultant de la réfraction de la lumière à travers l'atmosphère d'une planète.", correct: false },
            { text: "Une illusion d'optique causée par la diffraction de la lumière stellaire à travers des nuages de gaz interstellaires.", correct: false },
        ]
    },
    {
        question: "Quelle est la force qui maintient les planètes en orbite autour du Soleil ?",
        answers: [
            { text: "Magnétisme", correct: false },
            { text: "Force centrifuge", correct: false },
            { text: "Gravité", correct: true },
            { text: "Pression atmosphérique", correct: false },
        ]
    },
    {
        question: "Qui a proposé la théorie du Big Bang pour expliquer l'origine de l'univers ?",
        answers: [
            { text: "Albert Einstein", correct: false },
            { text: "Georges Lemaître", correct: true },
            { text: "Edwin Hubble", correct: false },
            { text: "Stephen Hawking", correct: false },
        ]
    },
    {
        question: "Nom du premier satellite artificiel lancé dans l'espace ?",
        answers: [
            { text: "Vanguard 1", correct: false },
            { text: "Explorer 1", correct: false },
            { text: "Telstar 1", correct: false },
            { text: "Spoutnik 1", correct: true },
        ]
    }

];


console.log(questions);


// fin tableau quiz





const questionElement = document.querySelector<HTMLElement>("#question");
const answerButtonsElement = document.querySelector<HTMLButtonElement>(".answer-buttons");
const nextButton = document.querySelector<HTMLButtonElement>("#next-btn");
const buttons = document.querySelectorAll<HTMLButtonElement>(".btn");


let currentQuestionIndex = 0;
let score = 0;
let aPasRepondu = true



function startQuiz() {
    currentQuestionIndex = 0;
    score = 0;
    showQuestion();
}


function showQuestion() {

    if (questionElement && answerButtonsElement && nextButton) {
        const currentQuestion = questions[currentQuestionIndex];
        questionElement.textContent = currentQuestion.question;

        answerButtonsElement.innerHTML = "";

        for (let answer of currentQuestion.answers) {
            const button:HTMLButtonElement = document.createElement("button");
            button.textContent = answer.text;
            button.classList.add("btn");
            button.addEventListener("click", () => {
                handleAnswerClick(answer, button);
            });
            answerButtonsElement.append(button);
        }


        nextButton.style.display = "none"; 
    }
}
nextButton.addEventListener("click", () => {
    console.log('next');
    
    currentQuestionIndex++;
    if (currentQuestionIndex < questions.length) {
        showQuestion();
    } else {
        console.log('là dedans');
        
        displayResult();
    }
    aPasRepondu = true
});

/**
 * Fonctin pour gérer le click sur une réponse
 * @param {Object} selectAnswer
 * @param {string} selectAnswer.text 
 * @param {boolean} selectAnswer.correct 
 * @param {HTMLButtonElement} button 
 */
function handleAnswerClick(selectAnswer:{text:string, correct:boolean}, button:HTMLElement) {

    buttons.forEach((btn) => {
        btn.disabled = true; 
    });
    if (aPasRepondu) {
        
        if (selectAnswer.correct) {
            button.style.backgroundColor = "green"; 
        score++;
        aPasRepondu =false
    } else {
        button.style.backgroundColor = "red";
        aPasRepondu =false
    }
    
}
    nextButton.style.display = "block"; 
}


function displayResult() {
    nextButton.style.display = "none";
    const quizBlock = document.querySelector<HTMLElement>(".quiz")
  

    const endScreen = document.querySelector("#end-screen");
    const finalScore = document.querySelector("#final-score");
    endScreen.classList.remove("hide");
    finalScore.textContent = "Votre score: " + score + "/10 !";

    const restartBtn:HTMLButtonElement = document.createElement("button");
            restartBtn.textContent = "recommencer";
            restartBtn.classList.add("restart-btn");
            restartBtn.addEventListener("click", () => {
                startQuiz();
            });
            quizBlock.append(restartBtn);
    
            restartBtn.addEventListener("click", () => {
                endScreen.classList.add("hide");
                restartBtn.classList.add("hide");
                startQuiz();
            });

}

startQuiz();
